*Welcome to the Image Cropper!*

**To use this app:**   
1) Download the files onto your computer.  
2) Open two different tabs in your terminal.  

- In first tab navigate to resizeMe/client and type `npm start`.  
- In second tab navigate to resizeMe/server and type `node .`  

3) Both the frontend and backend should be running now.  
4) Navigate to `localhost:3000` and use the app.  
5) Make sure the app is fullscreen and devtools are not open before loading the app.   

- If they are, close them and refresh the page.  
- You can open dev tools after the app has loaded.  
- This is so that things are properly positioned on the page.  

6) Click "Import New Image" to upload a new image.  

- The image size has to be 1024 x 1024 (otherwise it will tell you to fix it).  
- The file has to be of type image (otherwise it will tell you to fix it).  

7) Click through the various dimension tabs at the top and move your image into the yellow
dimension frame to crop that part of the image.  
8) Ensure the images in each dimension fit within the crop frame (otherwise it will tell you to fix it).  
9) Click "Crop and Save" to initiate the save process (a loading bar will show).  
10) Once the loading bar has disappeared, click through the dimension tabs to view the cropped images. It might take a second for the image in that particular dimension to load for the first time.  
10) Retry if you'd like.  

**Known Issues:**      
1) Images that are resized (using resize corners) do not crop properly (there is some issue with ratio handling)   

- The relevant section for adding a solution is commented as TODO. 
- I debated removing the resize functionality entirely before submitting, but I worked pretty hard on getting it working so I didn't want to remove it entirely.  

2) If your dev tools is open the overlays will get messed up due to the positioning of elements.  
3) Lag in loading images while clicking through tabs after cloudinary cropped images/urls are returned.  
4) Slight pixel crop errors due to rounding errors and sizes of borders of elements.  
5) Uploading the same image consecutively doesn't work since the state doesn't change for the imgUrls. Needs a refresh before working again.   
6) If resize or move are active and you go out of the main dotted container with your mouse the mouseup event is not registered and the state is maintained until you click again within that box.   
7) There's a brief instant after image upload where the image element is loaded without a source and shows "Resize me" alt text before the actual image loads.   

**Further improvements:**   
1) Reduce upload and showing times for cropped images. (Use multipart/form-data instead of base64 encoded url)
2) Design and UX enhancements.  
3) Testing! I have not written any unit tests for the code nor have I tried it in different browsers or screen sizes (See Below).  
4) Make the page responsive and not dependent on dev tools being open or not or any page size alterations.  
5) Error logging and general usage logging.  
6) Small fix -> Rename Component 'DimensionOverlay' and 'overlay' to 'CropFrame' and 'frame' to be more legible and understandable.  

**Example Tests I'd Write:**    

_Image Upload:_   
- Uploading an image that is too small
- Uploading an image that is too big
- Uploading an image of all the various input formats (jpg, png, tif, raw, gif)
- Uploading a text file or a javascript file

_Image Movement:_   
- Moving in any direction
- Test appropriate changes in top offset with mouse y coordinate diff
- Test appropriate changes in left offset with mouse x coordinate diff
- Test that image movement is not triggering the events for image resizing

_Mouse Movement:_   
- Test that diff calculations are being done accurately between mouse up and mouse down
- Test that mouse up and mouse down events are being registered accurately based on programmed clicks
- Test that parent mouse events are being propogated to children and handled in the right way

_Image Resize:_   
- Test the ratio of width to height changes based on mouse movements
- Test the click of a resize corner and the appropriate mouse event
- Test that image resizing is not triggering the events for image movement
- Test that images are not being stretching and are maintaining the same ratio

_Image Crop:_   
- Test that the crop indexes are accurate
- Test that the crop frame does not impact the calculations

_General State Changes:_   
- Check differences between saved state and unsaved and that the transition is happening 
properly between the two
- Check that no changes occur while the load bar is showing (i.e. loading is in progress)

_Backend:_    
- Test all possible error throwing scenarios
  - internet is down
  - invalid input format
  - error from cloudinary
  - misc crashes
- Test input is properly formatted: 
  - an array of objects (size = # dimensions) is sent
  - each array of objects has a 'url' field
- Test what happens when the Cloudinary API credentials are invalid
- Test what happens when Cloudinary is down or inaccessible (can just change the call to something invalid)

_Offset calculation:_   
- Check that the containers' and overlays' offsets are being calculated accurately in different screen sizes

_Cross-browser:_   
- Test on different browsers to see the behavior

_Responsive:_    
- Test on different screen sizes


