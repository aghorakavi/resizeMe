import React, { Component } from 'react';

// styles, configs + static assets 
import styles from './App.css';

// components
import ImageUploader from '../ImageUploader/ImageUploader.js';

export default class App extends Component {
  render() {
    return (
      <div className={styles.base}>
        <ImageUploader />
      </div>
    );
  }
}