import React, { Component } from 'react';
import PropTypes from 'prop-types';

// styles, configs + static assets 
import styles from './Button.css';

export default class Button extends Component {
  render() {
    const {onClick, styling, value} = this.props;
    return (
      <button className={styles[styling || 'base']} onClick={() => onClick()}>
      	{value}
      </button>
    );
  }
}

Button.propTypes = {
  onClick: PropTypes.func.isRequired,
	styling: PropTypes.string,
	value: PropTypes.string.isRequired
}