import React, { Component } from 'react';
import PropTypes from 'prop-types';

// styles, configs + static assets 
import styles from './InputField.css';

export default class InputField extends Component {
  render() {
    const {accept, onChange, placeholder, styling, type} = this.props;
    return (
      <input accept={accept} className={styles[styling]} onChange={(e) => onChange(e)}
      	placeholder={placeholder} type={type}> 
      </input>
    );
  }
}

InputField.propTypes = {
	accept: PropTypes.arrayOf(PropTypes.string),
	onChange: PropTypes.func,
	placeholder: PropTypes.string,
	styling: PropTypes.string,
	type: PropTypes.string,
}