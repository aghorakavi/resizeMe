import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import SkyLight from 'react-skylight';
import _ from 'lodash';

// styles, configs + static assets 
import styles from './ImageUploader.css';
import RESIZE_OPTIONS from '../../util/resizeOptions.js';
import HARDCODED_COPY from '../../util/hardcodedCopy.js';

// components
import ActionButtonsBar from './ActionButtonsBar/ActionButtonsBar.js';
import DimensionOptionsBar from './DimensionOptionsBar/DimensionOptionsBar.js';
import ImageCropper from './ImageCropper/ImageCropper.js';

// Global variables for the file
const HARDCODED_ERROR_COPY = HARDCODED_COPY.ImageUploader.errors;

export default class ImageUploader extends React.Component {
  /**
  * Initiates starting state
  * @constructor
  */
  constructor(props) {
    super(props);
    this.state = {
      activeResizeOptionIndex: 0,
      imgFile: '',
      imgPreviewUrl: '',
      resizeOptions: RESIZE_OPTIONS,
      locationsArray: this.createInitialLocationsArray(),
      saveInitiated: false
    };
  }

  /**
  * Resets state to before the save was initiated because of invalid crops
  */
  cancelImageSave(indices) {
    const {resizeOptions} = this.state,
      status = 'cropNotWithinLimits',
      errorDescriptor = _.join(_.map(indices, (i) => {
        const {name, dimensions} = resizeOptions[i],
          [width, height] = dimensions;
        return name + ' (' + width + ' x ' + height + ')';
      }), ', ') + '.';
    
    this.setState({
      saveInitiated: false,
      savedCloudImageUrls: null,
      error: {
        status: status,
        text: HARDCODED_ERROR_COPY[status] + errorDescriptor
      }
    }, this.dialog.show());
  }


  /**
  * Creates array of objects that maintain location of each image in a particular crop dimension
  */
  createInitialLocationsArray(width, height) {
    return _.map(_.range(RESIZE_OPTIONS.length), () => {
      return {
        height: height ||  1024,
        width: width || 1024,
        left: 0,
        top: 0,
        x: null,
        y: null
      };
    })
  }

  /**
  * Handles import of new image
  * @param {Event} e - Click event triggered on the import image button
  */
  handleImageChange(e) {
    e.preventDefault();
    var self = this,
      status;
    
    // Read the file uploaded
    const reader = new FileReader(),
      file = e.target.files[0],
      cancelSaveSettings = {
        imgFile: '',
        imgPreviewUrl: '',
        saveInitiated: false,
        savedCloudImageUrls: null,
        activeResizeOptionIndex: 0,
        locationsArray: self.createInitialLocationsArray(),
      };

    // Check that something was actually uploaded
    if (file) {
      reader.readAsDataURL(file);
      // Check that an image was uploaded
      if (file.type.indexOf('image') < 0) {
        status = 'invalidUploadType';
        self.setState(_.assign(cancelSaveSettings, {
          error: {
            status: status,
            text: HARDCODED_ERROR_COPY[status]
          }
        }), self.dialog.show());
      }
      else {
        // Load the image and format it appropriately
        reader.onloadend = () => {
          let dataUrl = reader.result,
            imageObj = new Image(),
            width, height;
          imageObj.src = dataUrl;

          imageObj.onload = function() {
            width = imageObj.width;
            height = imageObj.height;

            // Check that the image is of appropriate size
            if (!_.isEqual(width, 1024) || !_.isEqual(height, 1024)) {
              status = 'imageIncorrectSize';
              // dimensions are too large show the dialog and do nothing else
              self.setState(_.assign(cancelSaveSettings, {
                error: {
                  status: status,
                  text: HARDCODED_ERROR_COPY[status]
                }
              }), self.dialog.show());
            }
            // Save the new image to the state
            else {
              self.setState({
                imgFile: file,
                imgPreviewUrl: dataUrl,
                imgWidth: width,
                imgHeight: height,
                saveInitiated: false,
                savedCloudImageUrls: null,
                activeResizeOptionIndex: 0,
                locationsArray: self.createInitialLocationsArray(width, height)
              });
            }
          }
        }
      }
    }
  }

  /**
  * Initiate frontend saving process of cropped images
  * @param {Event} e - Click event triggered on the Crop and Save button click
  */
  handleCropUploadStart(e) {
    this.setState({
      saveInitiated: true
    })
  }

  /**
  * Handle backend save of cropped images
  * @param {Array} locationsArray - Array of objects, one object for each resize
  *                                 dimension with cropped image url + offsets
  */
  handleCroppedImagesSave(locationsArray) {
    const self = this,
      currentState = this.state,
      {resizeOptions} = currentState,
      arrayRightLength = _.isEqual(locationsArray.length, resizeOptions.length),
      arrayItemsCorrectType = _.every(locationsArray, Object),
      arrayItemsContainImgUrl = _.every(locationsArray, (obj) => {return _.has(obj, 'url');}),
      cancelSettings = {
        saveInitiated: false,
        savedCloudImageUrls: null
      };

    // Checks if locationsArray exists and is properly formatted before sending data to backend
    if (locationsArray && arrayRightLength && arrayItemsCorrectType && arrayItemsContainImgUrl) {
      axios.post('/save', {
        fileName: currentState.imgFile.name,
        previewUrl: currentState.imgPreviewUrl,
        locationsArray: locationsArray
      })
      .then(res => {
        // If any of the urls came back as undefined reset state to before save
        if (!res.data || !_.isEqual(res.data.length, resizeOptions.length)) {
          self.setState(_.assign(cancelSettings, {
            error: {
              status: 500,
              text: HARDCODED_ERROR_COPY[500]
            }
          }), this.dialog.show())
        }
        // Update state with cloudinary cropped image urls
        self.setState({
          savedCloudImageUrls: res.data
        });
      })
      // In case of error reset state to before save
      .catch(err => {
        const {response} = err,
          {status} = response;

        self.setState(_.assign(cancelSettings, {
          error: {
            status: status,
            text: HARDCODED_ERROR_COPY[status]
          }
        }), this.dialog.show());
      })
    }
    // In case of incorrectly formatted locationsArray reset state to before save
    else {
      self.setState(cancelSettings, this.dialog.show());
    }
  }

  /**
  * Save currently active crop resize dimension option 
  * @param {Event} e - Click event triggered on the resize dimension option click
  */
  handleResizeOptionClick(i) {
    this.setState({
      activeResizeOptionIndex: i
    });
  }

  /**
  * Save left and top offsets of each dimension crop guide overlay
  * @param {DOMNode} containerNode - Outer container DOM node in which the resizable
  *                                  and movable image resides
  */
  saveOverlayOffsets(containerNode) {
    // Get node offsets of container
    const rect = ReactDOM.findDOMNode(containerNode).getBoundingClientRect(),
      containerWidth = rect.width,
      containerHeight = rect.height;
    var overlayOffsets, width, height;

    // Calculate all the overlay offsets for each resize dimension
    overlayOffsets = _.map(this.state.resizeOptions, (option) => {
      [width, height] = option.dimensions;
      return {
        left: (containerWidth - width)/2,
        top: (containerHeight - height)/2
      }
    });

    this.setState({
      overlayOffsets: overlayOffsets
    });
  }

  /**
  * Update the currently active resize option location offsets
  * @param {Object} newAttrsObject - Object of updates to previous image location
  */
  updateLocationObject(newAttrsObject) {
    const {locationsArray, activeResizeOptionIndex} = this.state,
      newLocation = _.assign(locationsArray[activeResizeOptionIndex], newAttrsObject);

    // Set the newly updated location object to the state
    locationsArray[activeResizeOptionIndex] = newLocation;
    this.setState({
      locationsArray: locationsArray
    });
  }

  render() {
    const {state} = this,
      {error, imgPreviewUrl, savedCloudImageUrls, saveInitiated} = state,
      loading = saveInitiated && !savedCloudImageUrls;
    return (
      <div className={styles.base}>
        <DimensionOptionsBar options={RESIZE_OPTIONS} state={state}
          onOptionClick={(i) => this.handleResizeOptionClick(i)}/>
        <SkyLight hideOnOverlayClicked dialogStyles={{height: '100px'}}
          ref={ref => this.dialog = ref} title={HARDCODED_ERROR_COPY.title}>
          {error && error.text}
        </SkyLight>
        <ImageCropper state={state}
          cancelImageSave={(indices) => this.cancelImageSave(indices)}
          saveCroppedImages={(locations) => this.handleCroppedImagesSave(locations)}
          saveOverlayOffsets={(name, node) => this.saveOverlayOffsets(name, node)}
          updateLocation={(obj) => this.updateLocationObject(obj)}
        />
        <ActionButtonsBar hidden={loading} imgPreviewUrl={imgPreviewUrl}
          savedCloudImageUrls={savedCloudImageUrls}
          handleImageChange={(e) => this.handleImageChange(e)}
          handleCropUploadStart={(e) => this.handleCropUploadStart(e)}
        />
      </div>
    )
  }
}

