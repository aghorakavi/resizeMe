import React, { Component } from 'react';
import PropTypes from 'prop-types';

// styles, configs + static assets 
import styles from './ActionButtonsBar.css';

// components
import Button from '../../Common/Button/Button.js';
import InputField from '../../Common/InputField/InputField.js';

export default class ActionButtonsBar extends Component {
  render() {
    const {handleCropUploadStart, handleImageChange, hidden, imgPreviewUrl, savedCloudImageUrls} = this.props;
    return (
      <div className={styles[hidden ? 'hidden' : 'buttonBar']}>
        <label className={styles[imgPreviewUrl ? 'fileInputMask' : 'fileInputMaskBold']}>
          Import New Image
          <InputField styling={'hiddenFileInput'} type='file' onChange={(e) => handleImageChange(e)} />
        </label>
        {imgPreviewUrl && 
          <Button styling={savedCloudImageUrls ? 'hidden' : 'submit'} onClick={(e) => handleCropUploadStart(e)} value='Crop and Save'/>
        }
      </div>
    );
  }
}

ActionButtonsBar.propTypes = {
  handleCropUploadStart: PropTypes.func.isRequired,
  handleImageChange: PropTypes.func.isRequired,
  hidden: PropTypes.bool.isRequired,
  imgPreviewUrl: PropTypes.string.isRequired
}