import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

// styles, configs + static assets 
import styles from './DimensionOption.css';

export default class DimensionOption extends Component {
  render() {
  	const {index, option, onClick, state} = this.props,
      {dimensions, name} = option,
      [width, height] = dimensions,
      {activeResizeOptionIndex} = state,
      isCurrentlyActiveStep = _.isEqual(activeResizeOptionIndex, index);

    return (
      <div className={styles[isCurrentlyActiveStep ? 'active' : 'base']}
        onClick={(i) => onClick(index)}>
      	<p className={styles.name}>{name}</p>
      	<p className={styles.dimensions}>{width + ' x ' + height}</p>
      </div>
    );
  }
}

Image.propTypes = {
  index: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
	option: PropTypes.shape({
		name: PropTypes.string.isRequired,
		dimensions: PropTypes.arrayOf(PropTypes.number).isRequired,
	}).isRequired,
  state: PropTypes.object.isRequired
}