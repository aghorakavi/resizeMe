import React, { Component } from 'react';
import PropTypes from 'prop-types';

// styles, configs + static assets 
import styles from './DimensionOptionsBar.css';

// components
import DimensionOption from './DimensionOption/DimensionOption.js';

export default class DimensionOptionsBar extends Component {
  render() {
    const {onOptionClick, options} = this.props;
    return (
      <div className={styles.base}>
      	{options.map((option, i) =>
          <DimensionOption onClick={(i) => onOptionClick(i)} key={i} index={i}
            option={option} state={this.props.state}
          />
        )}
      </div>
    );
  }
}

DimensionOptionsBar.propTypes = {
  onOptionClick: PropTypes.func.isRequired,
	options: PropTypes.arrayOf(PropTypes.shape({
		name: PropTypes.string,
		dimensions: PropTypes.arrayOf(PropTypes.number)
	})).isRequired
}