import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

// styles, configs + static assets 
import styles from './DimensionOverlay.css';

export default class DimensionOverlay extends Component {
  /**
  * Conver string to camelCase
  * @param {String} str - String to convert into camelCase
  */
  _camelize(str) {
    return str.replace(/(?:^\w|[A-Z]|\b\w)/g, function(letter, index) {
      return _.isEqual(index, 0) ? letter.toLowerCase() : letter.toUpperCase();
    }).replace(/\s+/g, '');
  }

  /**
  * Render the overlay on the canvas the first time
  */
  componentDidMount() {
    this.updateOverlay();
  }

  /**
  * Update the location of the overlay based on current resize option state
  */
  componentDidUpdate() {
    this.updateOverlay();
  }

  /**
  * Add appropriate overlay offsets for currently active dimension option
  */
  updateOverlay() {
    const {overlay} = this.refs,
      {state} = this.props,
      {activeResizeOptionIndex, overlayOffsets} = state,
      currentOverlay = overlayOffsets[activeResizeOptionIndex];

    overlay.style.left = currentOverlay.left + 'px';
    overlay.style.top = currentOverlay.top + 'px';
  }
  
  render() {
    const {chosenSize} = this.props;
    return (
      <div ref='overlay' className={styles[this._camelize(chosenSize.name) + 'Overlay']}>
      </div>
    );
  }
}

DimensionOverlay.propTypes = {
	chosenSize: PropTypes.shape({
    name: PropTypes.string.isRequired,
    dimensions: PropTypes.arrayOf(PropTypes.number)
  })
};