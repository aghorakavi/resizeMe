import React, { Component } from 'react';
import PropTypes from 'prop-types';

// styles, configs + static assets 
import styles from './ImageCropper.css';

// components
import DimensionOverlay from './DimensionOverlay/DimensionOverlay.js';
import ResizableContainer from './ResizableContainer/ResizableContainer.js';

export default class ImageCropper extends Component {
  /**
  * Calculate dimension overlay offsets based on newly mounted container
  */
  componentDidMount() {
    this.props.saveOverlayOffsets(this.refs.resizer);
  }

  render() {
    const {cancelImageSave, state, saveOverlayOffsets, saveCroppedImages, updateLocation} = this.props,
      {activeResizeOptionIndex, imgFile, resizeOptions, saveInitiated} = state,
      showOverlay = imgFile && !saveInitiated;

    return (
      <div ref='resizer' className={styles.base}>
        {showOverlay &&
          <DimensionOverlay chosenSize={resizeOptions[activeResizeOptionIndex]} state={state}
            saveOverlayOffsets={(name, node) => saveOverlayOffsets(name, node)}
          />
        }
        <ResizableContainer
          cancelImageSave={(indices) => cancelImageSave(indices)}
          saveCroppedImages={(locations) => saveCroppedImages(locations)}
          state={state}
          updateLocation={(obj) => updateLocation(obj)}
        />
      </div>
    );
  }
}

ImageCropper.propTypes = {
  cancelImageSave: PropTypes.func.isRequired,
  saveCroppedImages: PropTypes.func.isRequired,
  saveOverlayOffsets: PropTypes.func.isRequired,
  state: PropTypes.object.isRequired,
  updateLocation: PropTypes.func.isRequired
}