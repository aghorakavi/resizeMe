import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

// styles, configs + static assets 
import styles from './ImageCanvas.css';

// components
import ResizeCorner from '../ResizeCorner/ResizeCorner.js';

export default class Canvas extends React.Component {
  /**
  * Render the image on the canvas the first time
  */
  componentDidMount() {
    this.updateCanvas();
  }

  /**
  * Updates the canvas based on the current state/action
  * - If a crop and save was initiated:
  *  [Case 1] there are cloudinary cropped image urls that already generated
  *    =>  display the cropped images
  *  [Case 2] there are no cloudinary cropped image urls
  *    => crop the images and generate the based64 urls
  * - If a save was not initiated:
  *  [Case 3] It will update the canvas after any image move or resize
  */
  componentDidUpdate() {
    const {state} = this.props,
      {saveInitiated, savedCloudImageUrls} = state;

    if (saveInitiated) {
      if (savedCloudImageUrls) {
        // If cropped cloudinary images are returned, show them
        this.handleCroppedImageDisplay();
      } else {
        // Else crop the images 
        this.handleImageCrop();
      }
    }
    // Update images in edit mode
    else {
      this.updateCanvas();
    }
  }

  /**
  * [Case 1] A crop and save was initiated and cloudinary cropped image urls were generated
  *
  * Function finds the currently active image url and updates the image to display it
  * with the correct offsets
  */
  handleCroppedImageDisplay() {
    const {resizeImg, container} = this.refs,
      {activeResizeOptionIndex, overlayOffsets, savedCloudImageUrls} = this.props.state,
      currentOverlay = overlayOffsets[activeResizeOptionIndex],
      dataUrl = savedCloudImageUrls[activeResizeOptionIndex];

    // Set the image source to the cropped cloudinary returned image url
    resizeImg.src = dataUrl;
    container.style.left = currentOverlay.left + 'px';
    container.style.top = currentOverlay.top + 'px';
  }

  /**
  * [Case 2] A crop and save was initiated and no cloudinary cropped image urls exist
  *
  * Function generates base64 encoded urls of the cropped images that will be sent to the backend
  */
  handleImageCrop() {
    const {canvas, resizeImg} = this.refs,
      {cancelImageSave, saveCroppedImages, state} = this.props,
      {imgHeight, imgWidth, locationsArray, overlayOffsets, resizeOptions} = state,
      ctx = canvas.getContext('2d'),
      newLocations = [],
      invalidCropIndices = [];
    
    // For each dimension image crop selection
    _.each(resizeOptions, (option, i) => {
      let newDataUrl, left, top, width, height,
        currentLocation = locationsArray[i],
        overlayLocation = overlayOffsets[i],
        tooLeft, tooRight, tooUp, tooDown,
        heightRatio, widthRatio;

      // Calculate the cropping offsets
      left = overlayLocation.left - currentLocation.left;
      top = overlayLocation.top - currentLocation.top;
      [width, height] = option.dimensions;

      // Calculate whether any part of the crop box does not contain part of the image
      tooLeft = (left + width) > currentLocation.width;
      tooRight = currentLocation.left > overlayLocation.left;
      tooDown = currentLocation.top > overlayLocation.top;
      tooUp = (top + height) > currentLocation.height;

      // If any part of the crop box is empty and doesn't contain the picture, cancel save
      if (tooLeft || tooRight || tooUp || tooDown) {
        invalidCropIndices.push(i)
      }
      
      // Set canvas dimensions
      canvas.width = width;
      canvas.height = height;

      // Find the ratio between the current image size and original image size
      heightRatio = currentLocation.height / imgHeight;
      widthRatio = currentLocation.width / imgWidth;

      if (!_.isEqual(heightRatio, 1) || !_.isEqual(widthRatio,1)) {
        // TODO: Adjust cropping accordingly
        console.log('Ratios have changed. Adjust crops accordingly.');
      }

      // Crop the image and generate base64 url of it
      ctx.drawImage(resizeImg, left, top, width, height, 0, 0, width, height);
      newDataUrl = canvas.toDataURL();
      
      // Save the offsets and url of the cropped image
      newLocations.push({
        left: left,
        top: top,
        width: width,
        height: height,
        url: newDataUrl
      })
    });

    // If no crops had empty pixels between crop areas
    if (_.isEmpty(invalidCropIndices)) {
      // Save the cropped images to the backend
      saveCroppedImages(newLocations);
    } else {
      // Cancel the save
      cancelImageSave(invalidCropIndices);
    }
  }

  /**
  * [Case 3] Display a resized image or a moved image based on user mouse interactions
  */
  updateCanvas(i) {
    var newDataUrl, imageObj;
    const {canvas, resizeImg, container} = this.refs,
      ctx = canvas.getContext('2d'),
      {activeResizeOptionIndex, imgPreviewUrl, locationsArray} = this.props.state,
      location = locationsArray[i || activeResizeOptionIndex],
      width = location.width || imageObj.width,
      height = location.height || imageObj.height;

    // Create new image instance to draw onto canvas
    imageObj = new Image();
    imageObj.src = imgPreviewUrl;
    canvas.width = width;
    canvas.height = height;

    // Draw onto the canvas, generate image URL, and add proper container offsets
    imageObj.onload = function() {
      ctx.drawImage(imageObj, 0, 0, width, height);
      newDataUrl = canvas.toDataURL();
      newDataUrl && (resizeImg.src = newDataUrl);
      container.style.left = location.left + 'px';
      container.style.top = location.top + 'px';
    }
  }

  render() {
    const {handleImageMoveStart, handleResizeStart, state} = this.props,
      {savedCloudImageUrls, saveInitiated} = state,
      loading = saveInitiated && !savedCloudImageUrls;

    return(
      <div ref='container' className={styles.base}>
        <ResizeCorner onMouseDown={(e, direction) => handleResizeStart(e, direction)}
          direction={'nw'} hidden={saveInitiated} />
        <ResizeCorner onMouseDown={(e, direction) => handleResizeStart(e, direction)}
            direction={'ne'} hidden={saveInitiated} />
        <canvas ref='canvas' className={styles.hidden}></canvas>
        <img ref='resizeImg' className={styles[loading ? 'hidden' : 'img']} alt='Resize me' draggable={false}
          onMouseDown={(e) => handleImageMoveStart(e)}></img>
        <ResizeCorner onMouseDown={(e, direction) => handleResizeStart(e, direction)}
          direction={'se'} hidden={saveInitiated} />
        <ResizeCorner onMouseDown={(e, direction) => handleResizeStart(e, direction)}
            direction={'sw'} hidden={saveInitiated} />
      </div>
    )
  }
}

Canvas.propTypes = {
  cancelImageSave: PropTypes.func.isRequired,
  handleImageMoveStart: PropTypes.func.isRequired,
  handleResizeStart: PropTypes.func.isRequired,
  saveCroppedImages: PropTypes.func.isRequired,
  state: PropTypes.object.isRequired
}
