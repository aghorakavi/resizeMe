import React from 'react';
import { BarLoader } from 'react-spinners';
import PropTypes from 'prop-types';
import _ from 'lodash';

// styles, configs + static assets 
import styles from './ResizableContainer.css';

// components
import ImageCanvas from './ImageCanvas/ImageCanvas.js';

export default class ResizableContainer extends React.Component {
  /**
  * Initiates starting state
  * @constructor
  */
  constructor(props) {
    super(props);
    this.state = {
      moveActive: false,
      resizeActive: false
    };
  }

  /**
  * Calculates and moves image according to mouse move
  * @param {Event} e - Move event triggered on image mouse move 
  */
  handleImageMove(e) {
    const {state, updateLocation} = this.props,
      {activeResizeOptionIndex, locationsArray, saveInitiated} = state,
      {moveActive} = this.state;

    if (moveActive && !saveInitiated) {
      // Calculate difference in mouse location from when image move started to now
      const location = locationsArray[activeResizeOptionIndex],
        leftDiff = e.clientX - location.x,
        topDiff = e.clientY - location.y,
        newLeft = location.left + leftDiff,
        newTop = location.top + topDiff;

      updateLocation({
        left: newLeft,
        top: newTop,
        x: e.clientX,
        y: e.clientY
      });
    }
  }

  /**
  * Activates image move mode
  * @param {Event} e - Mouse down event triggered on image 
  */
  handleImageMoveStart(e) {
    const {state, updateLocation} = this.props,
      {saveInitiated} = state;

    if (!saveInitiated) {
      // Activate move mode and deactivate resizing
      this.setState({
        moveActive: true,
        resizeActive: false,
        resizeDirection: ''
      });

      // Update the location of where the cursor was when action began
      updateLocation({
        x: e.clientX,
        y: e.clientY
      });
    }
  }

  /**
  * Calls appropriate mouse move handling function based on current state
  * @param {Event} e - Mouse move event triggered on image or container 
  */
  handleMouseMove(e) {
    const {state} = this.props,
      {saveInitiated} = state,
      {resizeActive, moveActive} = this.state;

    if (!saveInitiated) {
      // Resizing is active
      if (resizeActive) {
        this.handleResize(e);
      }
      // Drag is active
      else if (moveActive) {
        this.handleImageMove(e);
      }
    }
  }

  /**
  * Deactivates all modifications of an image including resizing and moving
  * @param {Event} e - Mouse up event triggered on image or container 
  */
  handleMouseUp(e) {
    const {state} = this.props,
      {saveInitiated} = state;

    if (!saveInitiated) {
      this.setState({
        moveActive: false,
        resizeActive: false
      });
    }
  }

  /**
  * Calculates width, height and offsets of an image after a resize move event
  * @param {Event} e - Mouse move event triggered on image or container 
  */
  handleResize(e) {
    const {state} = this.props,
      {saveInitiated} = state,
      {resizeActive} = this.state;

    if (resizeActive && !saveInitiated) {
      const {clientX, clientY} = e,
        {resizeDirection} = this.state,
        {state, updateLocation} = this.props,
        {activeResizeOptionIndex, locationsArray} = state,
        location = locationsArray[activeResizeOptionIndex],
        xDiff = clientX - location.x,
        yDiff = clientY - location.y;
      var width, height, left, top;

      // Calculate differences in values based on resize direction
      if (_.isEqual(resizeDirection, 'ne')) {
        width = location.width + xDiff;
        height = location.height - yDiff;
        left = location.left;
        top = location.top + yDiff;
      } else if (_.isEqual(resizeDirection, 'nw')) {
        width = location.width - xDiff;
        height = location.height - yDiff;
        left = location.left + xDiff;
        top = location.top + yDiff;
      } else if (_.isEqual(resizeDirection, 'se')) {
        width = location.width + xDiff;
        height = location.height + yDiff;
        left = location.left;
        top = location.top;
      } else if (_.isEqual(resizeDirection, 'sw')) {
        width = location.width - xDiff;
        height = location.height + yDiff;
        left = location.left + xDiff;
        top = location.top;
      }

      // Default behavior is to maintain aspect ratio
      height = width / location.width * location.height;

      // Update the location coordinates of the current image
      updateLocation({
        width: width,
        height: height,
        left: left,
        top: top,
        x: clientX,
        y: clientY
      });
    }
  }

  /**
  * Activates resizing of an image
  * @param {Event} e - Mouse move event triggered on image or container 
  * @param {String} direction - Direction in which the resizing is happening
  *                             one of ['ne', 'nw', 'se', 'sw']
  */
  handleResizeStart(e, direction) {
    const {updateLocation, state} = this.props,
      {saveInitiated} = state;

    if (!saveInitiated) {
      // Activate resizing and deactivate moving
      this.setState({
        resizeActive: true,
        resizeDirection: direction,
        moveActive: false
      }, () => {return false;});

      // Update the location of where the cursor was when action began
      updateLocation({
        x: e.clientX,
        y: e.clientY
      });
    }
  }

  render() {
    const {cancelImageSave, saveCroppedImages, state} = this.props,
      {savedCloudImageUrls, saveInitiated} = state,
      loading = saveInitiated && !savedCloudImageUrls;

    return (
      <div ref='outer' className={styles.base}
        onMouseMove={(e) => this.handleMouseMove(e)}
        onMouseUp={(e) => this.handleMouseUp(e)}>
        <BarLoader color={'#123abc'} loading={loading} width={2000}/>
        {state.imgFile &&
          <ImageCanvas state={_.assign(this.state, state)}
            cancelImageSave={(indices) => cancelImageSave(indices)}
            handleImageMoveStart={(e) => this.handleImageMoveStart(e)}
            handleResizeStart={(e, direction) => this.handleResizeStart(e, direction)}
            saveCroppedImages={(locations) => saveCroppedImages(locations)}
          />
        }
      </div>
    )
  }
}

ResizableContainer.propTypes = {
  cancelImageSave: PropTypes.func.isRequired,
  saveCroppedImages: PropTypes.func.isRequired,
  state: PropTypes.object.isRequired
}