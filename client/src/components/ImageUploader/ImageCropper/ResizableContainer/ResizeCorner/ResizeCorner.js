import React from 'react';
import PropTypes from 'prop-types';

// styles, configs + static assets 
import styles from './ResizeCorner.css';

export default class ResizeCorner extends React.Component {
  render() {
    const {direction, hidden, onMouseDown} = this.props;
    return(
      <span onMouseDown={(e) => onMouseDown(e, direction)} className={styles[hidden ? 'hidden' : direction]}></span>
    )
  }
}

ResizeCorner.propTypes = {
	direction: PropTypes.string.isRequired,
	onMouseDown: PropTypes.func.isRequired
}