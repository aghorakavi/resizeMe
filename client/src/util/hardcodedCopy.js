var hardcodedCopyByComponent = {
	ImagePreview: {
		noPreviewImage: "Please upload an image to resize.",
		incorrectImageSize: "Image uploads must be a maximum of 1024 x 1024."
	},
  ImageUploader: {
    errors: {
      cropNotWithinLimits: 'The image is not within the cropping limits for: ',
      imageIncorrectSize: 'The image you\'re trying to upload is not the right size. It must have a size of 1024px x 1024px.',
      invalidUploadType: 'The file you are uploading is not an image. Please try again.',
      title: 'Uh oh, something went wrong!',
      400: 'Request was malformed. Please refresh and retry.',
      401: 'Looks like you\'re not authorized to do this action. Please refresh and retry.',
      403: 'Looks like you\'re not allowed to do this action. Please refresh and retry.',
      404: 'Looks like you\'re trying to do something we don\'t allow. Please refresh and retry.',
      499: 'Looks like the request timed out. Please check that you are connected to the internet' +
        ' and that your server is running. Refresh to retry.',
      500: 'Please check that you are connected to the internet and that your server is running. Refresh to retry.'
    }
  }
}

export default hardcodedCopyByComponent;