var resizeOptions = [
	{
		name: 'horizontal',
		dimensions:  [755, 450]
	},
	{
		name: 'vertical',
		dimensions: [365, 450]
	},
	{
		name: 'horizontal small',
		dimensions: [365, 212]
	},
	{
		name: 'gallery',
		dimensions: [380, 380]
	}
]

export default resizeOptions;