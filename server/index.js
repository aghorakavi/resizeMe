var express = require('express'),
  app = express(),
  async = require('async'),
  bodyParser = require('body-parser'),
  cloudinary = require('cloudinary'),
  _ = require('lodash');

// Increase the limit of incoming the request body
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({
    limit: '50mb',
    extended: true
  }));

app.post('/save', function (req, res) {
  var cloudinaryImageUrls = [];
  cloudinary.config({ 
    cloud_name: 'anuhyag', 
    api_key: '755757749783839', 
    api_secret: 'pUWYQ3i8xNoT16vCNj52TNmEr0Y' 
  });
  
  async.eachOfSeries(req.body.locationsArray, (location, index, callback) => {
    cloudinary.uploader.upload(location.url, function(result) { 
      const {error, url} = result;
      // cloudinary sends both error and response in result
      url && cloudinaryImageUrls.push(url);
      return callback(error);
    });
  }, function (err) {
    if (err) {
      res.status(err.http_code || 500).send();
    } else {
      res.send(cloudinaryImageUrls);
    }
  });
});

app.listen(5000, function () {
  console.log('App listening on port 5000!');
});

